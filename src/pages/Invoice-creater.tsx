"use client";   
import { Button, Card, CardContent, CardHeader, Container, IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import react, { useEffect, useRef, useState } from "react";
import { DeleteForever } from "@mui/icons-material";
import PrintIcon from '@mui/icons-material/Print';
import ReactToPrint, { useReactToPrint } from 'react-to-print';
import AddBoxIcon from '@mui/icons-material/AddBox';

// declare interface eachItem{
//     item:String,
//     qty:number,
//     price:number,
//     total:number,
// } 

export default function InvoiceCreator(){
    type Item ={
        item:String;
        qty:number;
        price:number;
        total:number;
    }

    const emptyItem:Item = {
        item:"",
        qty:0,
        price:0,
        total:0,
    }
    const [tdata,setTdata] = useState([emptyItem]);
    const [allTotal,setAllTotal] = useState(0);
    const [subTotal,setSubTotal] = useState(0);
    const [discount,setDiscount] = useState(0);
    const [discountAmt,setDiscountAmt] = useState(0);
    const [gst,setGst] = useState(0);
    const [gstAmt,setGstAmt] = useState(0);
    const componentRef= useRef(null);

    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
      });

    useEffect(()=>{
        updateDiscount(discount);
        updateGst(gst);
    },[subTotal]);


    useEffect(()=>{
        addAll();
    },[subTotal,discount,gst]);


    const addAll = ()=>{
        let at  = subTotal-discountAmt+gstAmt;
        setAllTotal(at);
    }

    const updateDiscount = (rate:number)=>{
        setDiscount(rate);
        let disAmt = (rate/100)*subTotal;
        setDiscountAmt(disAmt);
    }

    const updateGst = (rate:number)=>{
        setGst(rate);
        let tad  = subTotal-discountAmt;
        let ga = (rate/100)*tad;
        setGstAmt(ga);
    }

    const addSubTotal = (arrayData:Item[])=>{
        let total = 0;
        for (const item of arrayData) {
            total  = total+item.total;
        }

        setSubTotal(total);
    }


    const changeData = (index:number,key:keyof Item,value:any)=>{
        let allData:Item[]  = [...tdata];
        let eachitem:Item = allData[index];
        eachitem[key] = value;
        let total:number = eachitem.price*eachitem.qty;;
        eachitem.total  = total;
        allData[index] = eachitem;
        updateData(allData);
        
    }

    const updateData = (data:Item[])=>{
        setTdata(data);
        addSubTotal(data);
    }

    const addRow = ()=>{
        setTdata([...tdata,emptyItem]);
    }

    const removeRow = (index:number)=>{
        let allData  = [...tdata];
        allData.splice(index,1);
        updateData(allData);
    }
    
    return(
        <div style={{
            // backgroundColor:"red",
            // height:"100%"
        }}> 
            <Container
                maxWidth="xl"
                style={{
                    // backgroundColor:"#fff",
                    paddingTop:50,
                }}
            >

                <Card style={{
                    padding:20,
                }}>
                    <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Easy Invoice
                    </Typography>
                    <Button
                        color="success"
                        variant="contained"
                        style={{alignSelf:"flex-end",float:"right", marginBottom:10}}

                        onClick={()=>{
                            handlePrint();
                        }}
                    >
                        <PrintIcon/>
                    </Button>
                        <TableContainer component={Paper} ref={componentRef}  >
                            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                <TableHead>
                                    <TableRow
                                        
                                    >
                                        <TableCell>Sn</TableCell>
                                        <TableCell align="center">Item</TableCell>
                                        <TableCell align="left">Qty</TableCell>
                                        <TableCell align="left">Price</TableCell>
                                        <TableCell align="right">Total</TableCell>
                                        <TableCell align="right">Action</TableCell>
                                        <TableCell>
                                            {/* <Button>Delete</Button> */}
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        tdata.map((item,index)=>{
                                            console
                                            return(
                                                <TableRow key={index}>
                                                    <TableCell>{index+1}</TableCell>
                                                    <TableCell align="left">
                                                        <TextField
                                                        size="small"
                                                        id="outlined-required"
                                                        value={item.item}
                                                        onChange={(e)=>{
                                                            console.log(e.target.value)
                                                            changeData(index,"item",e.target.value)
                                                        }}
                                                        />
                                                    </TableCell>
                                                    <TableCell align="left">
                                                        <TextField
                                                        size="small"
                                                        id="outlined-required"
                                                        value={item.qty}
                                                        type="number"
                                                        onChange={(e)=>{
                                                            console.log(e.target.value)
                                                            changeData(index,"qty",e.target.value)
                                                        }}/>
                                                    </TableCell>
                                                    <TableCell align="left">
                                                        <TextField
                                                        size="small"
                                                        id="outlined-required"
                                                        value={item.price}
                                                        type="number"
                                                        onChange={(e)=>{
                                                            console.log(e.target.value)
                                                            changeData(index,"price",e.target.value)
                                                        }}/>
                                                    </TableCell>
                                                    <TableCell align="right">${item.total}</TableCell>
                                                    <TableCell align="right">
                                                        {tdata.length>1&&
                                                        <Button 
                                                            variant="contained"
                                                            color="error"
                                                            onClick={()=>{
                                                                removeRow(index);
                                                            }}
                                                        >
                                                            <DeleteForever/>
                                                        </Button>
                                                        }
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        })
                                    }
                                    <TableRow>
                                        <TableCell align="right" colSpan={4}>
                                            Sub Total
                                        </TableCell>
                                        <TableCell align="right">
                                            ${subTotal.toFixed(2)}
                                        </TableCell>
                                        <TableCell align="right">
                                            <Button
                                                variant="contained"
                                                onClick={()=>{
                                                    addRow();
                                                }}
                                            >
                                                <AddBoxIcon/>
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="right" colSpan={4}>
                                            Discount:
                                            <TextField
                                            size="small"
                                            id="outlined-required"
                                            value={discount}
                                            type="number"
                                            variant="standard"
                                            style={{
                                                width:50,
                                                marginLeft:10,
                                                textAlignLast:"end" ,
                                            }}
                                            onChange={(e)=>{
                                                console.log(e.target.value)
                                                updateDiscount(parseInt(e.target.value))
                                            }}/> %
                                        </TableCell>
                                        <TableCell align="right">
                                            - ${discountAmt.toFixed(2)}
                                        </TableCell>
                                        <TableCell align="right">
                                            
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="right" colSpan={4}>
                                            GST
                                            <TextField
                                            size="small"
                                            id="outlined-required"
                                            value={gst}
                                            type="number"
                                            variant="standard"
                                            style={{
                                                width:50,
                                                marginLeft:10,
                                                textAlignLast:"end" ,
                                            }}
                                            onChange={(e)=>{
                                                console.log(e.target.value)
                                                updateGst(parseInt(e.target.value))
                                            }}/> %
                                        </TableCell>
                                        <TableCell align="right">
                                            ${gstAmt.toFixed(2)}
                                        </TableCell>
                                        <TableCell/>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="right" colSpan={4}>
                                            <span
                                               className="title1"
                                            >
                                                Total
                                            </span>
                                        </TableCell>
                                        <TableCell align="right">
                                            ${allTotal.toFixed(2)}
                                        </TableCell>
                                        <TableCell/>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>

                    </CardContent>
                </Card>
            </Container>
        </div>
    )
}